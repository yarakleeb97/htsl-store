$(".navbar-nav .nav-link").on("click", function () {
  $(".navbar-nav").find(".active").removeClass("active");
  $(this).addClass("active");
});
$("#suggestions").owlCarousel({
  loop: true,
  margin: 4,
  nav: true,
  autoplay: true,
  dots: true,
  items:4,
  navText: [
    "<i class='bi bi-arrow-left-circle'></i>",
    "<i class='bi bi-arrow-right-circle'></i>",
  ],
});
$("#best-sales").owlCarousel({
  loop: true,
  margin: 4,
  nav: true,
  autoplay: true,
  dots: false,
  items:1,
  navText: [
    "<i class='bi bi-arrow-left-circle'></i>",
    "<i class='bi bi-arrow-right-circle'></i>",
  ],
});
$("#last-product").owlCarousel({
  loop: true,
  margin: 4,
  nav: true,
  autoplay: true,
  dots: false,
  items:1,
  navText: [
    "<i class='bi bi-arrow-left-circle'></i>",
    "<i class='bi bi-arrow-right-circle'></i>",
  ],
});
$("#top-rate-sales").owlCarousel({
  loop: true,
  margin: 4,
  nav: true,
  autoplay: true,
  dots: false,
  items:1,
  navText: [
    "<i class='bi bi-arrow-left-circle'></i>",
    "<i class='bi bi-arrow-right-circle'></i>",
  ],
});
